<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.04.17
 * Time: 11:40
 */
use interfaces\ConnectionInterface;

class DataExchange1C implements \interfaces\DataExchangeInterface
{
    private $user_id;
    private $client;

    public function __construct()
    {
        $this->client = new SoapConnectionClass();

        if (!$this->client->isConnected())
            $this->client = new SoapConnectionClass();
    }

    public function setUserId($id)
    {
        if (!is_int($id))
            throw new \exceptions\Exchange1cException();

        $this->user_id = $id;
    }

    public function getUserLogin()
    {
        // TODO: Implement getUserLogin() method.
    }

    public function getUserOrders()
    {
        // TODO: Implement getUserOrders() method.
    }
}