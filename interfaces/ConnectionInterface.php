<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.04.17
 * Time: 10:53
 */

namespace interfaces;


interface ConnectionInterface
{
    public function isConnected();

    public function getConnectionType();
}