<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.04.17
 * Time: 11:08
 */

namespace interfaces;


interface DataExchangeInterface
{
    public function setUserId($id);

    public function getUserLogin();

    public function getUserOrders();
}