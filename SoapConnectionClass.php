<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.04.17
 * Time: 11:15
 */
class SoapConnectionClass implements \interfaces\ConnectionInterface
{
    const CONNECTION_TIMEOUT = 7;

    public function __construct()
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        ini_set('default_socket_timeout', self::CONNECTION_TIMEOUT);

        //$client = new SoapClient("http://31.13.133.108:81/SiteExchange/ws/SiteEx.1cws?wsdl", array('login' => "registr", 'password' => "registr", 'exceptions' => 1, 'trace' => 1 )); // фиктивный адрес, для проверки ошибок подключения
        //$client = new SoapClient("http://31.13.133.10:81/SiteExchange/ws/SiteEx.1cws?wsdl", array('login' => "registr", 'password' => "registr", 'connection_timeout'=>$timeoutconnection));
    }

    public function isConnected()
    {
        // TODO: Implement isConnected() method.
        return true;
    }

    public function getConnectionType()
    {
        return 'soap';
    }
}